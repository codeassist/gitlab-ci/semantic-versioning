# Gitlab CI configuration for Semantic Versioning

See a base idea of how to integrate into Gitlab pipeline
[here](https://threedots.tech/post/automatic-semantic-versioning-in-gitlab-ci/).

### Usage
1. Prepare `DEPLOY_KEY` variable on the project (or group) level where you're
going to use 'semantic versioning' step as a part of the pipeline.
This variable must contain `private` part of the RSA key which will be
configured in `Settings` -> `CI/CD` -> `Deploy Keys`.

2. Enable chosen `Deploy Key` and obviously(!) set the checkbox
`Write access allowed`. Otherwise `gitlab-runner` won't be able to push tags
back to the Gitlab instance.

3. Then put the following snippet into your project `.gitlab-ci.yml` file:
```yaml
---
# As described in:
#   * https://docs.gitlab.com/ce/ci/yaml/#include
include:
  - remote: https://gitlab.com/codeassist/gitlab-ci/semantic-versioning/-/raw/master/.semver-ci.yml

stages:
  - test
  - versioning
  - release

# then, for example, place the next step when you want to tag your release
increment:tag:
  # add section from included file to the current job
  extends: .semantic_versioning
  # add missing config or override, if necessary, pre-configured keys right in the job config below
  stage: versioning
  when: manual
  only:
    - master
```