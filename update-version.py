import argparse
import logging
import re
import semver
import shlex
import subprocess
import sys


class LogFilter(logging.Filter):
    """
        Filters (passes through) all messages with level < LEVEL
    """
    def __init__(self, level):
        super(LogFilter).__init__()
        self.level = level

    def filter(self, record):
        return record.levelno < self.level


# see: https://docs.python.org/3.6/library/logging.html
def get_logger(name=None, level=logging.getLevelName(logging.WARNING)):
    """
    If a name is specified this function initializes global logger object with
    specified level (WARNING, by default)
    :param name: the name of the logger
    :param level: desired severity level, all messages lower than specified
    won't be shown
    :return: object of logging.getLogger(name) class, otherwise None object
    """
    if name is not None:
        # init logger object (singleton)
        logger = logging.getLogger(name)
        # After calling `getLogger()`, the desired level must be set by calling
        # logger.setLevel(). If this is not done, then only messages higher
        # than default WARNING level will be seen.
        # NOTE: this should not be done for root logger otherwise plenty of
        # system messages appear in the user's console
        logger.setLevel(level)

        if not logger.handlers:
            # If this evaluates to false, logging messages are not passed by
            # this logger or by its child loggers to the
            # handlers of higher level (ancestor) loggers. The constructor sets
            # this attribute to 1.
            logger.propagate = 0

            ####################
            # Console handlers #
            ####################
            # specify console output message's format
            console_formatter = logging.Formatter(
                '%(asctime)s | %(funcName)-16.16s | '
                '%(levelname)-8.8s | %(message)s')
            # init handlers to stream messages into system output/error
            stdout_handler = logging.StreamHandler(sys.stdout)
            stdout_handler.flush = sys.stdout.flush

            stderr_handler = logging.StreamHandler(sys.stderr)
            stderr_handler.flush = sys.stderr.flush

            # set different log-levels for every stream and add filter if
            # necessary so any message appears only in the single stream
            stdout_handler.setLevel(logging.DEBUG)
            # messages lower than WARNING go to stdout
            stdout_handler.addFilter(LogFilter(logging.WARNING))
            stdout_handler.setFormatter(console_formatter)

            # messages higher or equal WARNING go to stderr
            stderr_handler.setLevel(max(logging.DEBUG, logging.WARNING))
            stderr_handler.setFormatter(console_formatter)

            # add console logging handlers
            logger.addHandler(stdout_handler)
            logger.addHandler(stderr_handler)

        return logger
    else:
        return None


class Transformer(object):

    _transformation_map = {
        'from_http_with_auth_to_git': {
            'src_pattern': 'http(s)?://(.+)@([^/]+)(/(.+))+',
            'dst_pattern': 'git@\\3:\\5'
        },
        'from_http_without_auth_to_git': {
            'src_pattern': 'http(s)?://([^/]+)(/(.+))+',
            'dst_pattern': 'git@\\2:\\4'
        },
        'from_http_without_auth_to_host': {
            'src_pattern': '^http(s)?://([^/]+)(/.*)*',
            'dst_pattern': '\\2'
        }
    }

    def __init__(self, url, direction=None):
        self.url = url.strip("'").strip('"')
        self.direction = direction

    def transform(self):
        transformed_url = ""
        try:
            # transform in a way as direction is set
            if self.direction is not None:
                transformed_url = re.sub(r'{src_pattern}'
                                         .format(src_pattern=self._transformation_map[self.direction]['src_pattern']),
                                         r'{dst_pattern}'
                                         .format(dst_pattern=self._transformation_map[self.direction]['dst_pattern']),
                                         self.url,
                                         flags=re.IGNORECASE)
            # if direction is not explicitly set, then try to find out passed URL category
            else:
                for direction_name, direction_definition in self._transformation_map.items():
                    # if URL is matched against any of available patterns
                    if re.fullmatch(r'{pattern}'.format(pattern=direction_definition['src_pattern']), self.url,
                                    flags=re.IGNORECASE) is not None:
                        # then transform via this combination and break the loop
                        transformed_url = re.sub(r'{src_pattern}'
                                                 .format(src_pattern=direction_definition['src_pattern']),
                                                 r'{dst_pattern}'
                                                 .format(dst_pattern=direction_definition['dst_pattern']),
                                                 self.url,
                                                 flags=re.IGNORECASE)
                        break

            # if the pattern wasn't matched, check if string is returned unchanged and replace by empty string
            if transformed_url.lower() == self.url.lower():
                transformed_url = ""
        except AttributeError:
            return ""

        return transformed_url


# global scope
app_logger = get_logger(__name__, 'INFO')


def run_shell_command(cmd):
    out = None
    err = None
    command_line_args = shlex.split(cmd)

    app_logger.debug('Executing [{command}]'
                     .format(command=' '.join([str(arg) for arg in command_line_args])))

    try:
        shell_subprocess = subprocess.Popen(
            command_line_args,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        # Popen.communicate() will set the 'returncode' attribute when it's done(*).
        (out, err) = shell_subprocess.communicate()

    except OSError as ose:
        app_logger.exception('OS exception occurred: ' + str(ose))
        return 1
    except subprocess.TimeoutExpired as te:
        app_logger.exception('Timeout exception occurred: ' + str(te))
        return 1
    except subprocess.CalledProcessError as cpe:
        app_logger.exception('Process exception occurred: ' + str(cpe))
        return 1
    else:
        # Popen.returncode
        #   The child return code, set by poll() and wait() (and indirectly by communicate()).
        #   A 'None' value indicates that the process has not terminated yet.
        #   A negative value '-N' indicates that the child was terminated by signal N (Unix only).
        if shell_subprocess.returncode is None:
            # shell subprocess is alive, let's wait
            shell_subprocess.wait()

            # if none of exception was raised print the result and return the code from the function
        if out is not None and out:
            out = ('\n'.join(list(msg.decode('utf-8') for msg in out.split(b'\n')))).strip()
            app_logger.info(out)
        else:
            out = ''
        if err is not None and err:
            err = ('\n'.join(list(msg.decode('utf-8') for msg in err.split(b'\n')))).strip()
            app_logger.error(err)
        else:
            err = ''

        return shell_subprocess.returncode, out, err


def bump_version(**argv):
    ver = semver.VersionInfo.parse(argv['current_tag'])

    if argv['bump_type'].lower() == 'major':
        return str(ver.bump_major())
    elif argv['bump_type'].lower() == 'minor':
        return str(ver.bump_minor())
    else:
        return str(ver.bump_patch())


def tag_repo(**argv):
    # Transforms the repository URL to the SSH URL or distinguish host from the URL
    #   * Example input: https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/threedotslabs/ci-examples.git
    #     Example output: git@gitlab.com:threedotslabs/ci-examples.git
    #   * Example input: https://gitlab.com/threedotslabs/ci-examples.git
    #     Example output: gitlab.com
    push_url = Transformer(url=argv['url']).transform()

    if push_url != '':
        if run_shell_command('git remote set-url --push origin {push_url}'.format(push_url=push_url))[0] == 0:
            if run_shell_command('git tag {tag}'.format(tag=argv['tag']))[0] == 0:
                app_logger.info('pushing {tag} to the remote {remote_url}'.format(tag=argv['tag'], remote_url=push_url))
                if run_shell_command('git push origin {tag}'.format(tag=argv['tag']))[0] == 0:
                    return True
    else:
        app_logger.fatal("couldn't parse the remote registry url {url}, exiting...!".format(url=argv['url']))

    return False


def main(**argv):
    update_repo = run_shell_command('git fetch --tags')
    if update_repo[0] != 0:
        return 1

    last_commit = run_shell_command('git show --pretty=format:%s -s HEAD')

    if last_commit[0] == 0:
        app_logger.info('the last commit is [{last_commit}]'
                        .format(last_commit=last_commit[1]))

    # define conditions for `major` version bump
    if 'major' in last_commit[1].lower():
        bump_type = 'major'
    # define conditions for `minor` version bump
    elif 'feature' in last_commit[1].lower() or 'minor' in last_commit[1].lower():
        bump_type = 'minor'
    else:
        bump_type = 'patch'

    current_tag = run_shell_command('git describe --tags')
    if current_tag[0] == 0:
        # skip already tagged commits
        if '-' not in current_tag[1]:
            app_logger.info('this commit has already been tagged with [{current_tag}]'
                            .format(current_tag=current_tag[1]))
            return 0
    else:
        # assign variable identical structure as `run_shell_command` returns
        current_tag = (0, '0.0.0', '')
    # sometimes simple `git describe --tags` can't detect the latest tag, so getting revision and passing it as
    # an argument helps to make a comparison between them in that case
    tagged_revision = run_shell_command('git rev-list --tags --max-count=1')
    if tagged_revision[0] == 0 and re.match('^[A-Z0-9a-z]+$', tagged_revision[1]) is not None:
        latest_tag = run_shell_command('git describe --tags {revision}'.format(revision=tagged_revision[1]))
    else:
        # assign variable identical structure as `run_shell_command` returns
        latest_tag = (0, '0.0.0', '')
    app_logger.info('the latest tag found is [{latest_tag}]'
                    .format(latest_tag=latest_tag[1]))

    # if a tag couldn't be properly detected then bumped version won't be over the one stored in latest_tag,
    # then the next_tag must be calculated relative to the latest_tag, not described
    latest_tag_ver = semver.VersionInfo.parse(latest_tag[1])
    if latest_tag_ver.compare(bump_version(current_tag=current_tag[1], bump_type=bump_type)) != -1:
        next_tag = bump_version(current_tag=latest_tag[1], bump_type=bump_type)
    else:
        # if everything was detected fine, then simply bump relative to the stdout of git command
        next_tag = bump_version(current_tag=current_tag[1], bump_type=bump_type)

    app_logger.info('tagging current commit with [{next_tag}] tag (where update type is [{bump_type}])'
                    .format(next_tag=next_tag, bump_type=bump_type))
    if tag_repo(url=argv['url'], tag=next_tag):
        return 0
    else:
        return 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-u', '--url',
                        help='The string representation of remote repository\'s URL that needs to be transformed.',
                        required=True)

    args = parser.parse_args()

    sys.exit(main(url=args.url))
